# gitea actions guide

versions:

- gitea: 1.20.0-rc2
- act_runner: 0.2.0

## windows

1. register runner image

```pwsh
act_runner generate-config > config-win.yaml
act_runner '-c' 'config-win.yaml' register '--instance' 'http://127.0.0.1:3000' '--token' '<your token>' '--labels=windows-latest:host' '--name=self-host' '--no-interactive'
```

if you wan't make the runner also works for linux, the `--labels` shoud be:

`--labels=windows-latest:host,ubuntu-latest:docker://catthehacker/ubuntu:pwsh-latest`

The `--labels` can be docker container (but doesn't work in current releases of act_runner):

- `windows-latest:host` works well, but needs specific shell to `pwsh` or `powershell` in action step explicit
- `windows-latest:docker://mcr.microsoft.com/windows/nanoserver:ltsc2019` on win10 build 19045.x
- `windows-latest:docker://mcr.microsoft.com/windows/nanoserver:ltsc2022` on win11 build 22621.x

2. start runner

```pwsh
act_runner -c config-win.yaml daemon
```

